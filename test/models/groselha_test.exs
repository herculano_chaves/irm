defmodule InstitutoRioModa.GroselhaTest do
  use InstitutoRioModa.ModelCase

  alias InstitutoRioModa.Groselha

  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Groselha.changeset(%Groselha{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Groselha.changeset(%Groselha{}, @invalid_attrs)
    refute changeset.valid?
  end
end
