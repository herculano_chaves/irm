ExUnit.start

Mix.Task.run "ecto.create", ~w(-r InstitutoRioModa.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r InstitutoRioModa.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(InstitutoRioModa.Repo)

