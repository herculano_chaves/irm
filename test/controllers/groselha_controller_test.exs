defmodule InstitutoRioModa.GroselhaControllerTest do
  use InstitutoRioModa.ConnCase

  alias InstitutoRioModa.Groselha
  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, groselha_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing groselhas"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, groselha_path(conn, :new)
    assert html_response(conn, 200) =~ "New groselha"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, groselha_path(conn, :create), groselha: @valid_attrs
    assert redirected_to(conn) == groselha_path(conn, :index)
    assert Repo.get_by(Groselha, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, groselha_path(conn, :create), groselha: @invalid_attrs
    assert html_response(conn, 200) =~ "New groselha"
  end

  test "shows chosen resource", %{conn: conn} do
    groselha = Repo.insert! %Groselha{}
    conn = get conn, groselha_path(conn, :show, groselha)
    assert html_response(conn, 200) =~ "Show groselha"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, groselha_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    groselha = Repo.insert! %Groselha{}
    conn = get conn, groselha_path(conn, :edit, groselha)
    assert html_response(conn, 200) =~ "Edit groselha"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    groselha = Repo.insert! %Groselha{}
    conn = put conn, groselha_path(conn, :update, groselha), groselha: @valid_attrs
    assert redirected_to(conn) == groselha_path(conn, :show, groselha)
    assert Repo.get_by(Groselha, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    groselha = Repo.insert! %Groselha{}
    conn = put conn, groselha_path(conn, :update, groselha), groselha: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit groselha"
  end

  test "deletes chosen resource", %{conn: conn} do
    groselha = Repo.insert! %Groselha{}
    conn = delete conn, groselha_path(conn, :delete, groselha)
    assert redirected_to(conn) == groselha_path(conn, :index)
    refute Repo.get(Groselha, groselha.id)
  end
end
