defmodule InstitutoRioModa.Router do
  use InstitutoRioModa.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", InstitutoRioModa do
    pipe_through :browser # Use the default browser stack
    
    resources "/groselhas", GroselhaController

    resources "users", UserController

    get "/teste", PageController, :test

    get "/evento/:slug", ProductController, :show
    
    get "/fac", PageController, :fac

    get "/:slug", ProductController, :cat
    
    get "/", PageController, :index


  end

  # Other scopes may use custom stacks.
  # scope "/api", InstitutoRioModa do
  #   pipe_through :api
  # end
end
