defmodule InstitutoRioModa.LayoutView do
  use InstitutoRioModa.Web, :view
  alias InstitutoRioModa.Repo
	alias InstitutoRioModa.Category
  def categories(conn) do
		conn
		|> Repo.all(Category)
	end
end
