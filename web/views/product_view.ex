defmodule InstitutoRioModa.ProductView do
  use InstitutoRioModa.Web, :view
  alias InstitutoRioModa.Product

  def full_name(product) do
  	if product.name == "", do: product.title.name, else: product.name
  end
end
