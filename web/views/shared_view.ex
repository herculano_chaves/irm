defmodule InstitutoRioModa.SharedView do
	use InstitutoRioModa.Web, :view
	
	alias InstitutoRioModa.Repo
	alias InstitutoRioModa.Category

  def handler_info(conn) do
  	"Requisição de controlador #{controller_module conn} ação #{action_name conn}"
  end

  def categories(conn) do
   	Repo.all(Category)
  end

	def cool(conn) do
		var = "really works"
	end

end