defmodule InstitutoRioModa.PageView do
  use InstitutoRioModa.Web, :view
  alias InstitutoRioModa.Product

  def full_name(product) do
    if product.name == "", do: product.title.name, else: product.name
  end

  def print_category(product) do
    Map.get(product.category || %{}, :name, "Não tem categoria")
  end

  def print_facilitator(product) do
    Map.get(product.facilitator || %{}, :name, "Não tem")
  end
end
