defmodule InstitutoRioModa.City do
	use InstitutoRioModa.Web, :model

    schema "cities" do
		field :name, :string
		has_many :products, InstitutoRioModa.Product
	end

	@required_fields ~w(name)
  	@optional_fields ~w()
end