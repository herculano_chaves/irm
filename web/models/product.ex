defmodule InstitutoRioModa.Product do
  use InstitutoRioModa.Web, :model

  use Ecto.Model
  import Ecto.Query

  schema "products" do
    field :name, :string
    field :address, :string
    field :district, :string
    field :slug, :string
    belongs_to :category, InstitutoRioModa.Category
    belongs_to :facilitator, InstitutoRioModa.Facilitator
    belongs_to :title, InstitutoRioModa.Title
    belongs_to :city, InstitutoRioModa.City
    timestamps
  end

  @required_fields ~w(name address district)
  @optional_fields ~w()

  def for_category_slug(query, slug) do
    from p in query,
    join: c in assoc(p, :category),
    where: c.slug == ^slug
  end

  def order_by_name(query) do
    from p in query,
    order_by: [asc: p.name]
  end


  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end


end
