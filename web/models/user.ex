defmodule InstitutoRioModa.User do
  use InstitutoRioModa.Web, :model

  schema "users" do
    field :username, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :email, :string, unique: true

    timestamps
  end

  @required_fields ~w(username email)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:email)
    |> validate_format(:email, ~r/@/)
  end

  def registration_changeset(model, params) do
    model
    |> changeset(params)
    |> cast(params, ~w(password), [])
    |> validate_length(:password, min: 4, max: 10)
    |> put_pass_hash()
  end

  def put_pass_hash(changeset) do
    case changeset do
       %Ecto.changeset{valid?: true, changes: %{password: pass}} -> 
        put_change(changeset,:password_hash, Comeonin.Bcrypt.hashpwsalt(pass))
       ->
        _
        changeset
    end
  end

  def most_recent(query) do
    from p in query,
    order_by: [desc: p.id], limit: 10
  end
end
