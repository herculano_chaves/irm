defmodule InstitutoRioModa.Title do
	use InstitutoRioModa.Web, :model
  schema "titles" do
    field :name, :string
  
    has_many :products, InstitutoRioModa.Product 
    timestamps
  end

  @required_fields ~w(name)
  
end