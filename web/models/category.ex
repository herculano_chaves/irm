defmodule InstitutoRioModa.Category do
  use InstitutoRioModa.Web, :model

  schema "categories" do
    field :name, :string
    field :slug, :string
    has_many :products, InstitutoRioModa.Product
    timestamps 
  end

  @required_fields ~w(name)
  @optional_fields ~w(slug)


  def for_slug(query, slug) do
    from c in query,
    where: c.slug == ^slug
  end

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end


end
