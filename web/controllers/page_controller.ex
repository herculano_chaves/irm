defmodule InstitutoRioModa.PageController do
  use InstitutoRioModa.Web, :controller

  alias InstitutoRioModa.Product
  alias InstitutoRioModa.Facilitator

  def index(conn, _params) do
  	products = Product
    |> Product.order_by_name 
    |> Repo.all
    |> Repo.preload(:facilitator)
    |> Repo.preload(:category)
    |> Repo.preload(:city)
    |> Repo.preload(:title)
  
    render(conn, "index.html", products: products)
  end
  def test(conn, _params) do
  	render conn, "teste.html"
  end
  def fac(conn, _params) do
  	facilitators = Repo.all(Facilitator)
  	render(conn, "facilitadores.html", facilitators: facilitators)	
  end
end
