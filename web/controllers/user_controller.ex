defmodule InstitutoRioModa.UserController do
	use InstitutoRioModa.Web, :controller

	alias InstitutoRioModa.User

	def index(conn, _params) do
		users = User
		|> User.most_recent
		|> Repo.all

		render(conn, "index.html", users: users)
	end

	def new(conn, _params) do
		changeset = User.changeset(%User{})
		render(conn, "new.html", changeset: changeset) 
	end

	def create(conn, %{"user" => user_params}) do
		changeset = User.registration_changeset(%User{}, user_params)

		case Repo.insert(changeset) do
			{:ok, _user}->
			 	conn
			 	|> put_flash(:info, "Usuário inserido com sucesso")
			 	|> redirect(to: user_path(conn, :index))
			{:error, changeset}->
				render(conn, "new.html", changeset: changeset)			
		end
	end

	def delete(conn, %{"id"=>id}) do
		user = Repo.get!(User, id)

		Repo.delete!(user)
		conn
		|> put_flash(:info, "Usuário deletado com sucesso!")
		|> redirect(to: user_path(conn, :index))
	end
end