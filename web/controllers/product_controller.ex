defmodule InstitutoRioModa.ProductController do
	use InstitutoRioModa.Web, :controller

	alias InstitutoRioModa.Product
	alias InstitutoRioModa.Category

	def show(conn, %{"slug" => slug}) do
		query = from p in Product, where: p.slug == ^slug 
 		product = Repo.one(query)
		render(conn, "show.html", product: product)
	end
	
	def cat(conn, %{"slug" => slug}) do
		category = Category |> Category.for_slug(slug) |> Repo.one 
		products = Product 
		|> Product.for_category_slug(slug) 
		|> Repo.all 
		|> Repo.preload([:category])
		|> Repo.preload([:title])

		render(conn, "category.html", products: products, category: category)		
	end

	
end