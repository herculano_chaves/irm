defmodule InstitutoRioModa.GroselhaController do
  use InstitutoRioModa.Web, :controller

  alias InstitutoRioModa.Groselha

  plug :scrub_params, "groselha" when action in [:create, :update]

  def index(conn, _params) do
    groselhas = Repo.all(Groselha)
    render(conn, "index.html", groselhas: groselhas)
  end

  def new(conn, _params) do
    changeset = Groselha.changeset(%Groselha{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"groselha" => groselha_params}) do
    changeset = Groselha.changeset(%Groselha{}, groselha_params)

    case Repo.insert(changeset) do
      {:ok, _groselha} ->
        conn
        |> put_flash(:info, "Groselha created successfully.")
        |> redirect(to: groselha_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    groselha = Repo.get!(Groselha, id)
    render(conn, "show.html", groselha: groselha)
  end

  def edit(conn, %{"id" => id}) do
    groselha = Repo.get!(Groselha, id)
    changeset = Groselha.changeset(groselha)
    render(conn, "edit.html", groselha: groselha, changeset: changeset)
  end

  def update(conn, %{"id" => id, "groselha" => groselha_params}) do
    groselha = Repo.get!(Groselha, id)
    changeset = Groselha.changeset(groselha, groselha_params)

    case Repo.update(changeset) do
      {:ok, groselha} ->
        conn
        |> put_flash(:info, "Groselha updated successfully.")
        |> redirect(to: groselha_path(conn, :show, groselha))
      {:error, changeset} ->
        render(conn, "edit.html", groselha: groselha, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    groselha = Repo.get!(Groselha, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(groselha)

    conn
    |> put_flash(:info, "Groselha deleted successfully.")
    |> redirect(to: groselha_path(conn, :index))
  end
end
