defmodule InstitutoRioModa.Repo.Migrations.CreateGroselha do
  use Ecto.Migration

  def change do
    create table(:groselhas) do
      add :name, :string

      timestamps
    end

  end
end
